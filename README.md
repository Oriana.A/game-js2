# Game project



This project goal is to create a game using JS without any framework. This exercise will consolidate my understanding and use of JS, simple algorithms,DOM and POO.



### Project instructions



- Choose the game theme and genre
- Make 2 or 3 mockups
- Create a board in gitlab with milestone and issues
- The project require the use of  JS objects
- Some datas has to be modified during the game (lifebar, score, progress...)
- The game is not necessarily responsive



### About the game

The goal of the game is to reach the oscar first. If you answer right, your player will move. If you answer wrongly to the question, the other player will move.

### Mockups

![](Maquettes/Maquette1.png)





![](Maquettes/UseCaseDiagram.png)







![](Maquettes/Maquette2.png)

### Review

![](Maquettes/code1.png)



I used an addEventListener on the Start button from the Home page and then used a display block and  remplaceChild to switch from the Home page to the Game Screen on the click.

<img src="Maquettes/Capture-du-2019-10-07-21-45-42-ConvertImage.png" style="zoom: 67%;" />

<img src="Maquettes/Capture-du-2019-10-07-21-45-46-ConvertImage.png" style="zoom:67%;" />



I made an array of object in which I created several instances of the class Quizz to store the different data for each instances.

![](Maquettes/Capture-du-2019-10-07-21-38-14-ConvertImage.png)



Then I wrote a function that put the question and answers of each instance of Quizz on different HTML element.  The loop is here to got through the array of answers of each instance.

![](Maquettes/Capture-du-2019-10-07-21-38-46-ConvertImage.png)

### Built with

![](https://img.icons8.com/dusk/64/000000/css3.png)![](https://img.icons8.com/color/96/000000/javascript.png)![](https://img.icons8.com/dusk/64/000000/html-5.png)





#### More

Author : Oriana

Date : 2019-08-05 - 2019-09-06