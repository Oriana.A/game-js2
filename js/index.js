let gameBoard = document.querySelector('#game-board');
let buttonStart = document.querySelector('button');
gameBoard.style.display = "none";
let opponent = document.querySelector("#pnj");
let oscar = document.querySelector("#oscar");
let player = document.querySelector("#player");
let quizzBoard = document.querySelector('#quizz-board');



//Change Play to Level 1

buttonStart.addEventListener('click', function () {
	let gameWindowStart = document.querySelector('.game-window-start');
	gameBoard.style.display = "block";
	gameWindowStart.parentNode.replaceChild(gameBoard, gameWindowStart);
});


//init object globally

function init() {

	opponent = document.querySelector("#opponent");
	opponent.style.position = 'relative';
	opponent.style.left = '0px';
	opponent.style.top = '290px';

	player = document.querySelector("#player");
	player.style.position = 'relative';
	player.style.left = '0px';
	player.style.top = '361px';

	oscar = document.querySelector("#oscar");
	oscar.style.position = 'absolute';
	oscar.style.left = '550px';
	oscar.style.top = '278px';
}

window.onload = init;


let data = [
	new Quizz("Who played Legolas ?", ["Lee Pace", "Orlando Bloom", "Luke Evans", "Chris Evans"], "Orlando Bloom"),
	new Quizz("When was The Avengers released ?", ["2012", "2019", "2010", "2014"], "2012"),
	new Quizz("What is the name of Harry Potter'school ?", ["Ogwart", "Haugwarts", "Hogwarts", "Awkward"], "Hogwarts"),
	new Quizz("In 'The Little Mermaid' what does Ariel use to brush her hair ?", ["a brush", "Her father's trident", "Sebastian'pincer", "a fork"], "a fork"),
	new Quizz("Who directed Kill Bill ?", ["Quentin Tarantino", "Steven Spielberg", "Georges Lucas", "Luc Besson"], "Quentin Tarantino"),
	new Quizz("Who played Legolas ?", ["Lee Pace", "Orlando Bloom", "Luke Evans", "Chris Evans"], "Orlando Bloom"),
	new Quizz("Who played Legolas ?", ["Lee Pace", "Orlando Bloom", "Luke Evans", "Chris Evans"], "Orlando Bloom"),
	new Quizz("Who played Legolas ?", ["Lee Pace", "Orlando Bloom", "Luke Evans", "Chris Evans"], "Orlando Bloom"),
	new Quizz("Who played Legolas ?", ["Lee Pace", "Orlando Bloom", "Luke Evans", "Chris Evans"], "Orlando Bloom"),
	new Quizz("Who played Legolas ?", ["Lee Pace", "Orlando Bloom", "Luke Evans", "Chris Evans"], "Orlando Bloom")
];


//défini un index pour le tableau data
let quesindex = 0;

function giveQues(quesindex) {
	//function qui display questioni
	let question = document.querySelector(".question");
	question.innerHTML = data[quesindex].text;

	//boucle classique sur les answer de la question actuellei
	for (let i = 0; i < data[quesindex].choices.length; i++) {
		let choice = document.querySelector("#answer" + i);
		choice.innerHTML = data[quesindex].choices[i];
	}

};

giveQues(0);


function nextques() {

	let value = document.querySelector(".form-check-input:checked+.form-check-label").textContent;
	if (value === data[quesindex].answer) {
		console.log('YOUPI');
		player.style.left = parseInt(player.style.left) + 15 + 'px';

	} else {
		console.log('BOUH');
		opponent.style.left = parseInt(player.style.left) + 15 + 'px';
	}
	quesindex++;
	giveQues(quesindex);

}
